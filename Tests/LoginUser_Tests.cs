﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class LoginUser_Tests
    {
        [TestMethod]
        public void Login_LoginUser_CorrectLogin_bool()
        {
            LoginComponent.ILoginDataMapper fdm = new FakeLoginDataMapper();
            LoginComponent.Login login = new LoginComponent.Login(fdm);
            bool result = login.LoginUser("garrit", "test1234");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Login_LoginUser_NullPassword_bool()
        {
            LoginComponent.ILoginDataMapper fdm = new FakeLoginDataMapper();
            LoginComponent.Login login = new LoginComponent.Login(fdm);
            bool result = login.LoginUser("garrit", null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Login_LoginUser_NullUsername_bool()
        {
            LoginComponent.ILoginDataMapper fdm = new FakeLoginDataMapper();
            LoginComponent.Login login = new LoginComponent.Login(fdm);
            bool result = login.LoginUser(null, "test1234");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Login_LoginUser_WrongLogin_bool()
        {
            LoginComponent.ILoginDataMapper fdm = new FakeLoginDataMapper();
            LoginComponent.Login login = new LoginComponent.Login(fdm);
            bool result = login.LoginUser("garrit", "asdqwewetygfd");
            Assert.IsFalse(result);
        }
    }
}
