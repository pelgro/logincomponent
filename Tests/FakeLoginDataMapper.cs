﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoginComponent;

namespace Tests
{
    class FakeLoginDataMapper : ILoginDataMapper
    {
        public void CreateUser(User u)
        {
            
        }

        public void DeleteUser(User u)
        {
            
        }

        public bool LoginUser(User u)
        {
            if (u.Username == "garrit" && u.PasswordHash == "16d7a4fca7442dda3ad93c9a726597e4")
                return true;
            return false;
        }

        public bool UserIsUnique(User u)
        {
            if (u.Username == "peter")
                return false;
            return true;
        }


    }
}
