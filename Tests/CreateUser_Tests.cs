﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class CreateUser_Tests
    {
        [TestMethod]
        public void Login_CreateUser_AllDataOk_Void()
        {
            FakeLoginDataMapper fmd = new FakeLoginDataMapper();
            LoginComponent.Login login = new LoginComponent.Login(fmd);
            login.CreateUser("garrit", "123456", "123456");
            Assert.IsTrue(true);
        }

        /// <summary>
        /// A passwords needs to be greater than 5 characters
        /// </summary>
        [TestMethod]
        public void Login_CreateUser_ShortPassword_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser("garrit", "12345", "12345");
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }

        [TestMethod]
        public void Login_CreateUser_PasswordNotEqual_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser("garrit", "1234567", "1234568");
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }

        [TestMethod]
        public void Login_CreateUser_NullPassword_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser("garrit", null, null);
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }

        /// <summary>
        /// A username needs to be greater than 3 characters
        /// </summary>
        [TestMethod]
        public void Login_CreateUser_ShortUsername_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser("asd", "123456", "123456");
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }

        [TestMethod]
        public void Login_CreateUser_NullUsername_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser(null, "123456", "123456");
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }

        [TestMethod]
        public void Login_CreateUser_UsernameNotUnique_Exception()
        {
            bool failed = false;
            try
            {
                FakeLoginDataMapper fmd = new FakeLoginDataMapper();
                LoginComponent.Login login = new LoginComponent.Login(fmd);
                login.CreateUser("peter", "123456", "123456");
            }
            catch (Exception)
            {
                failed = true;
            }
            Assert.IsTrue(failed);
        }
    }
}
