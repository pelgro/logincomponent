﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoginComponent;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ILoginDataMapper db = new InMemoryStorage();
                Login login = new Login(db);
                login.CreateUser("garrit", "test1234", "test1234");
                login.CreateUser("peter", "test1234", "test1234");
                login.CreateUser("asdfg", "test1234", "test1234");
                login.CreateUser("qwerty", "test1234", "test1234");
                System.Console.WriteLine(login.LoginUser("garrit", "test1234"));
                System.Console.ReadKey();
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                System.Console.ReadKey();
            }
            
        }
    }
}
