﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginComponent
{
    public class InMemoryStorage : ILoginDataMapper
    {
        List<User> userList = new List<User>();
        public void CreateUser(User u)
        {
            userList.Add(u);
        }

        public void DeleteUser(User u)
        {
            userList.Remove(u);
        }

        public bool LoginUser(User u)
        {
            return userList.Exists(user => user.Username.ToLower() == u.Username.ToLower() && user.PasswordHash == u.PasswordHash);
        }

        public bool UserIsUnique(User u)
        {
            return !userList.Exists(user => user.Username.ToLower() == u.Username.ToLower());
        }
    }
}
