﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginComponent
{
    class CreateUserCommand : ICommand
    {

        private string _username;
        private string _password;
        private string _confirmPassword;
        private ILoginDataMapper _ldm;

        public CreateUserCommand(string username, string password, string confirmPassword, ILoginDataMapper ldm)
        {
            this._username = username;
            this._password = password;
            this._confirmPassword = confirmPassword;
            this._ldm = ldm;
        }
        public void Execute()
        {
            Helper.checkUsername(this._username);
            Helper.checkPassword(this._password);
            if (this._password != this._confirmPassword)
                throw new Exception("The passwords are not equal");
            User u = new User(_username, Helper.HashPassword(_password));
            if (!this._ldm.UserIsUnique(u))
                throw new Exception("User is not unique");
            this._ldm.CreateUser(u);
        }
    }
}
