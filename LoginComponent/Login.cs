﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginComponent
{
    public class Login
    {
        private ILoginDataMapper _ldm;

        public Login(ILoginDataMapper ldm)
        {
            this._ldm = ldm;
        }

        public void CreateUser(string username, string password, string confirmPassword)
        {
            new CreateUserCommand(username, password, confirmPassword, _ldm).Execute();
        }

        public bool LoginUser(string username, string password)
        {
            try
            {
                Helper.checkUsername(username);
                Helper.checkPassword(password);
                User u = new User(username, Helper.HashPassword(password));
                return this._ldm.LoginUser(u);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
