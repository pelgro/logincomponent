﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginComponent
{
    public interface ILoginDataMapper
    {
        void CreateUser(User u);
        bool UserIsUnique(User u);
        bool LoginUser(User u);
        void DeleteUser(User u);
    }
}
