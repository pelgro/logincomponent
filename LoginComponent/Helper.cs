﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace LoginComponent
{
    public class Helper
    {
        public static void checkUsername(string username)
        {
            if (username == null)
                throw new ArgumentNullException("Username is null");
            if (username.Length < 4)
                throw new Exception("Username is less than 4 characters");
        }

        public static void checkPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException("Password is null");
            if (password.Length < 6)
                throw new Exception("Password is less than 6 characters");
        }

        public static string HashPassword(string password)
        {
            MD5 md5 = MD5.Create();

            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(password));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
